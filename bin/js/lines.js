/* ------------------------------------------------------------

Lines_JS

-------------------------------------------------------------*/


function Lines_JS( place, options ){
	
	var lines_js = this;
	
	this.options = options;
	this.place_str = place;
	this.place = document.getElementById( place );
	

	this.W = this.place.offsetWidth * options.zoom;
	this.H = this.place.offsetHeight * options.zoom;

	console.log( this.W + " | " + this.H );
	
	this.LH = options.linkDistance;
	this.DS = this.LH/4;
	
	if( createjs && this.place ){
		
		// DOM init
		this.canvas = document.createElement( "canvas" );
		this.canvas.id = this.place_str + "_canvas";

    this.canvas.width =  this.W; 
    this.canvas.height =  this.H; 

    this.canvas.style.position = "absolute";
    this.canvas.style.left = "0px";
    this.canvas.style.top = "0px";
    this.canvas.style.width =   this.W / options.zoom + "px";
    this.canvas.style.height =   this.H / options.zoom + "px";

    this.canvas.style.opacity = 0;		

    this.place.appendChild( this.canvas );

		// creatJS init
		this.stage = new createjs.Stage( this.place_str + "_canvas" );
		
		// init
		
		this.dots = [];
		this.lines = [];
		
		var i;
		console.log(">>>>" + this.options.dotNumber);
		for( i = 0; i < this.options.dotNumber; i++ ){
			this.createDot();
		}

		this.line_sh = new createjs.Shape();
		this.stage.addChild( this.line_sh );
		
		this.timerId = null;
		
		this.status = 1;
		
		console.log( "Lines_JS init" );

	}else{
		console.log( "need CrateJS lib or parent element" );
	}
	
}

Lines_JS.prototype.faderAnimation = function(){
	var lines_js = this;
	if( this.canvas.style.opacity < this.options.opacity ){
		this.canvas.style.opacity = Number(this.canvas.style.opacity) + 0.005;
		this.faderTimeout = setTimeout( function(){
			lines_js.faderAnimation();
		},30);
	}
}


Lines_JS.prototype.start = function(){
	
	var lines_js = this;
	
	if( this.status == 1 ){
		createjs.Ticker.setFPS(60);
		createjs.Ticker.addEventListener( "tick", function(){ lines_js.render() });
		timerId = setTimeout( function(){ lines_js.calculate() }, 10 );
		this.stage.update();
		console.log( "Lines_JS start" );
		
		this.faderTimeout = setTimeout( function(){
			lines_js.faderAnimation();
		},30);
	}
	
}

Lines_JS.prototype.calculate = function(){

	var lines_js = this;
	
	this.lines.length = 0;
	
	for( var i = 0; i < this.dots.length ; i++ ){
		
		this.dots[i].x += this.dots[i].speed * Math.sin( Math.PI/180 * this.dots[i].r );
		this.dots[i].y += this.dots[i].speed * Math.cos( Math.PI/180 * this.dots[i].r );

		
		if( ( this.dots[i].y < - this.DS ) || ( this.dots[i].y > this.H + this.DS ) ){
			this.dots[i].r *= -1;
			this.dots[i].r -= 180;
		}
		
		if( ( this.dots[i].x < -this.DS ) ||  ( this.dots[i].x > this.W + this.DS )){
			this.dots[i].r *= -1;
		}

		
		for( var j = 0; j < this.dots.length ; j++ ){
			
			if( j!=i ){
				
				var a = this.dots[i].x - this.dots[j].x;
				var b = this.dots[i].y - this.dots[j].y;
				var c = Math.sqrt( a*a + b*b );

				if(  c < this.LH - 5 ){
					
					this.lines.push([
						this.dots[i].x,
						this.dots[i].y,
						this.dots[j].x,
						this.dots[j].y,
						this.dots[i].d*( this.LH - c )/this.LH, ( this.LH - c )/this.LH
						]);					
				}
				
			}
		}
	}
	
	this.timerId = setTimeout( function(){ lines_js.calculate() }, 10 );
	
}

Lines_JS.prototype.render = function(){
	

	this.line_sh.graphics.clear();
	
	var i;
	
	if(this.options.dotSize > 0 ){
		for( i = 0; i < this.dots.length ; i++ ){
			this.line_sh.graphics.beginFill("#ffffff");
			this.line_sh.graphics.drawCircle( this.dots[i].x, this.dots[i].y, this.dots[i].d*1.5 );
			this.line_sh.graphics.endFill();
		}
	}

	for( i = 0; i < this.lines.length ; i++ ){
		this.line_sh.graphics.setStrokeStyle( this.lines[i][4] ).beginStroke( "rgba(255,255,255," + this.lines[i][5] + ")");
		this.line_sh.graphics.moveTo( this.lines[i][0], this.lines[i][1] );
		this.line_sh.graphics.lineTo( this.lines[i][2], this.lines[i][3] );
	}

	this.stage.update();

	
}

Lines_JS.prototype.createDot = function(){

	var dot = {};
	
	dot.speed = this.options.speedMin + Math.random()*( this.options.speedMax - this.options.speedMin );
	
	dot.r = Math.random()*360;
	
	dot.d = Math.random()*this.options.dotSize + 1;

	dot.x = Math.random() * this.W;
	dot.y = Math.random() * this.H;
	
	this.dots.push( dot );
	
}

Lines_JS.prototype.stop = function(){
	
}

Lines_JS.prototype.pause = function( toggle ){
	
}